#!/bin/bash

. ./set-env.sh

docker stack rm GITLAB || true
docker stack deploy --compose-file gitlab.yml GITLAB

# -----------------------------------------------------------------------------
# Following execution of this script you can perform the steps described below.
# -----------------------------------------------------------------------------
# Run "docker service ls" until you see something like the following:

# ID                  NAME                    MODE                REPLICAS            IMAGE                         PORTS
# l0xacek2x9ho        GITLAB_omnibus          replicated          1/1                 gitlab/gitlab-ee:latest       *:23->22/tcp, *:80->80/tcp, *:443->443/tcp
#
# Once you see 1/1 under REPLICAS you can proceed to execute the other scripts
# in this directory.  I recommend running the apply license script first.
# -----------------------------------------------------------------------------
