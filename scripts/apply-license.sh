#!/bin/bash

# -----------------------------------------------------------------------------
# This script assumes:
#   - Gitlab is Running
#   - curl is installed
#   - jq is installed
# -----------------------------------------------------------------------------

. ./set-env.sh

ACCESS_TOKEN=$(curl --insecure -d "grant_type=password&username=root&password=$GITLAB_ROOT_PASSWORD" -X POST "$EXTERNAL_URL/oauth/token" 2> /dev/null |jq --raw-output .access_token)
curl --insecure -X POST --header "Authorization: Bearer $ACCESS_TOKEN" "$EXTERNAL_URL/api/v4/license?license=$GITLAB_LICENSE"
