#!/bin/bash

# -----------------------------------------------------------------------------
# This script assumes:
#   - Gitlab is Running
#   - curl is installed
#   - jq is installed
# -----------------------------------------------------------------------------

. ./set-env.sh

ACCESS_TOKEN=$(curl --insecure -d "grant_type=password&username=root&password=$GITLAB_ROOT_PASSWORD" -X POST "$EXTERNAL_URL/oauth/token" 2> /dev/null |jq --raw-output .access_token)
GITLAB_TOKEN=$(curl --insecure --header "Authorization: Bearer $ACCESS_TOKEN" --form "token=$GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN" --form "description=$GITLAB_RUNNER_DESCRIPTION" --request POST "$EXTERNAL_URL/api/v4/runners" 2> /dev/null|jq --raw-output .token)

rm -rf $GITLAB_RUNNER_CONFIG_HOST_VOLUME/*
echo " [[runners]]" > $GITLAB_RUNNER_CONFIG_HOST_VOLUME/config.toml
echo "  name = \"$GITLAB_RUNNER_DESCRIPTION\"" >> $GITLAB_RUNNER_CONFIG_HOST_VOLUME/config.toml
echo "  url = \"$EXTERNAL_URL/\"" >> $GITLAB_RUNNER_CONFIG_HOST_VOLUME/config.toml
echo "  token = \"$GITLAB_TOKEN\"" >> $GITLAB_RUNNER_CONFIG_HOST_VOLUME/config.toml
echo "  executor = \"docker\"" >> $GITLAB_RUNNER_CONFIG_HOST_VOLUME/config.toml
echo "  [runners.docker]" >> $GITLAB_RUNNER_CONFIG_HOST_VOLUME/config.toml
echo "    tls_verify = false" >> $GITLAB_RUNNER_CONFIG_HOST_VOLUME/config.toml
echo "    image = \"docker\"" >> $GITLAB_RUNNER_CONFIG_HOST_VOLUME/config.toml
echo "    privileged = false" >> $GITLAB_RUNNER_CONFIG_HOST_VOLUME/config.toml
echo "    disable_cache = false" >> $GITLAB_RUNNER_CONFIG_HOST_VOLUME/config.toml
echo "    volumes = [\"/var/run/docker.sock:/var/run/docker.sock\", \"/cache\"]" >> $GITLAB_RUNNER_CONFIG_HOST_VOLUME/config.toml
echo "    shm_size = 0" >> $GITLAB_RUNNER_CONFIG_HOST_VOLUME/config.toml
echo "  [runners.cache]" >> $GITLAB_RUNNER_CONFIG_HOST_VOLUME/config.toml

docker stack rm GITLAB_RUNNER || true
docker stack deploy --compose-file gitlab-runner.yml GITLAB_RUNNER
