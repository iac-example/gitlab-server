#!/bin/bash

# -----------------------------------------------------------------------------
# This script assumes:
#   - Gitlab is Running
#   - curl is installed
#   - jq is installed
# -----------------------------------------------------------------------------

. ./set-env.sh

GROUP_DEFINITION_FILE=../groups/shared_components.json

GROUP_DEFINITION=$(cat $GROUP_DEFINITION_FILE|jq '{name,path,description,visibility}')
ACCESS_TOKEN=$(curl --insecure -d "grant_type=password&username=root&password=$GITLAB_ROOT_PASSWORD" -X POST "$EXTERNAL_URL/oauth/token" 2> /dev/null |jq --raw-output .access_token)
GROUP_ID=$(curl --insecure -X POST -H "Authorization: Bearer $ACCESS_TOKEN" -H "Content-Type: application/json" -d "$GROUP_DEFINITION" "$EXTERNAL_URL/api/v4/groups" 2> /dev/null|jq .id)

ACCESS_LVL_LEN=$(cat $GROUP_DEFINITION_FILE|jq ".permissions|length")
CURRENT_LVL=0
while (( CURRENT_LVL < ACCESS_LVL_LEN )); do
  ACCESS_LVL=$(cat $GROUP_DEFINITION_FILE| jq -r ".permissions[$CURRENT_LVL].gitlabRoleLevel")
  AD_GROUP_LEN=$(cat $GROUP_DEFINITION_FILE| jq ".permissions[]|select(.gitlabRoleLevel == $ACCESS_LVL)|.activeDirectoryGroups|length")
  echo
  echo "Adding $AD_GROUP_LEN AD groups to Gitlab role level $ACCESS_LVL"
  CURRENT_GROUP=0
  while (( CURRENT_GROUP < AD_GROUP_LEN )); do
    AD_GROUP=$(cat $GROUP_DEFINITION_FILE| jq -r ".permissions[]|select(.gitlabRoleLevel == $ACCESS_LVL)|.activeDirectoryGroups|.[$CURRENT_GROUP]")
    curl --insecure --header "Authorization: Bearer $ACCESS_TOKEN" -d "cn=$AD_GROUP&group_access=$ACCESS_LVL&provider=ldapmain" -X POST "$EXTERNAL_URL/api/v4/groups/$GROUP_ID/ldap_group_links"
    let CURRENT_GROUP=CURRENT_GROUP+1
  done
  let CURRENT_LVL=CURRENT_LVL+1
done
